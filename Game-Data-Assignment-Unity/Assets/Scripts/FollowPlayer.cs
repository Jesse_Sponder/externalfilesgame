﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    Transform player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player").transform;
        else
            transform.position = new Vector3(player.position.x, player.position.y, -10);
    }
}
