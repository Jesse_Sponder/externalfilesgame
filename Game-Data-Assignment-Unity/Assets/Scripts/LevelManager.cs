﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour
{

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform powerupHolder;

    public enum MapDataFormat
    {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject powerupPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> powerupPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole()
    {
        //clear console readout
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    // destroy all gameobjects contained in a list (by looping through list and destroying children one by one
    // ...malicious
    static void DestroyChildren(Transform parent)
    {

        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel()
    {

        //clear console messages
        ClearEditorConsole();

        //destroy objects contained in object groundHolder
        DestroyChildren(groundHolder);

        //destroy objects contained in turretHolder
        DestroyChildren(turretHolder);

        DestroyChildren(powerupHolder);

        {
            // store path to map folder within streamingassets
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");

            // create path to specific map file by combining mapsDirectory with TMXFilename
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }



        {
            // clear int list mapData of entries
            mapData.Clear();

            // use previously established path to find map file and store contents in string content
            string content = File.ReadAllText(TMXFile);

            // create xmlreader to read allow access to specific attributes in xml formatted doc
            using (XmlReader reader = XmlReader.Create(new StringReader(content)))
            {

                //read up to element of name "map" - attributes of this element can now be accessed. no elements which occurred
                //previously in the doc can be accessed now
                reader.ReadToFollowing("map");

                //contains int value of width and height attributes in mapColumns and mapRows, 
                //converting the string value into and integer first
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                //Read to element "tileset," record size of tiles in tileWidthInPixels and tileHeightInPixels respectively
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                //Record spritesheet tile count and number of columns in spriteSheetTileCounts and spriteSheetColumns respectively
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));

                //calculate and record number of rows in spritesheet
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //read to "image" element in doc
                reader.ReadToFollowing("image");

                //create and record path to spritesheet by combining map path with source (image name) attribute in "image" element
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //read to the element "layer"
                reader.ReadToFollowing("layer");

                //read to the element "data" and records attribute "encoding" in encodingType
                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                //switches variable mapDataFormat to either base64 or csv based on encodingType
                switch (encodingType)
                {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                //records contents of element "data" in string variable mapDataString without whitespace infront or at the end
                mapDataString = reader.ReadElementContentAsString().Trim();

                //clear list of turretPositions
                turretPositions.Clear();

                //if objectgroup element exits, and if object element exists within: record x value and y value after calculating 
                //position in relation to tiles, then add vector posisiton to turretPositions list usign x and y values
                if (reader.ReadToFollowing("objectgroup"))
                    do
                    {
                        Debug.Log("passing through");
                        List<Vector3> objectPositions = new List<Vector3>();
                        string listName = reader.GetAttribute("name");
                        if (reader.ReadToDescendant("object"))
                        {
                            Debug.Log("in object");
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                objectPositions.Add(new Vector3(x, -y, 0));

                                //repeat while there is still an object element to read
                            } while (reader.ReadToNextSibling("object"));
                        }
                        if (listName == "Enemies")
                        {
                            Debug.Log("in enemies");
                            turretPositions = objectPositions;
                        }
                        else
                        {
                            powerupPositions = objectPositions;
                            Debug.Log("in powerups");
                        }

                    } while (reader.ReadToNextSibling("objectgroup"));

            }

            //based on the dataformat established earlier, convert data contents into something useable
            switch (mapDataFormat)
            {

                //if Base64
                case MapDataFormat.Base64:

                    //record converted byte array from mapDataString
                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    //begin loop for adding tileIds to mapData list by converting 4 bytes to an in and 
                    //proceeding by incrementing index by 4
                    while (index < bytes.Length)
                    {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;

                //if CSV
                case MapDataFormat.CSV:

                    //split single string content into seperate strings based on presence of " " char (split IDs into rows)
                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines)
                    {
                        //split single row strings into individual ID strings based on presence of "," char
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values)
                        {
                            //convert individual string IDs to integer values and add them to mapData
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //determine tilewidth and tileHeight in units based of tilewidthInPixels and tileHeightInPixels
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            //create tile offset vector for individual tiles based on tileWidth and tileHeight (it is centered by halving both values)
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);

            //create map offset vector using mapColumn number multiplied by tilewidth and halved (offsets to top left)
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // create a texture to load the spritesheet image at file location "spriteSheetFile" in, and set variables filtermode and 
        // wrapmode accordingly
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Clear mapSprites list then loop through spritesheet by row and column to create sprites
        // based on the recorded tileWidthInPixels and tileHeightInPixels with column and row used to create top left point of rect. pivot point is set to middle of sprite.
        // Sprite is then added to mapSprites
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--)
            {
                for (int x = 0; x < spriteSheetColumns; x++)
                {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }


        {

            tiles.Clear();

            // loop by number of mapRows and mapColumns 
            for (int y = 0; y < mapRows; y++)
            {
                for (int x = 0; x < mapColumns; x++)
                {

                    // create index for accessing individual tile IDs inside the mapData array using loop variables
                    int mapDatatIndex = x + (y * mapColumns);

                    //set up temp variable to contain tile ID
                    int tileID = mapData[mapDatatIndex];

                    //instantiate tile object from prefab using loop variables (map column and row numbers) to set position of tile in relation to 0,0 (which will be center of map)
                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;

                    //assign new tile object's sprite renderer corresponding sprite from mapSPrite list
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];

                    //make tile object a child of "groundHolder" object (for cleanliness)
                    tile.transform.parent = groundHolder;

                    //add tile object to "tiles" list
                    tiles.Add(tile);
                }
            }
        }


        // instantiate turret objects according to recorded turret positions from turret prefab
        {
            foreach (Vector3 turretPosition in turretPositions)
            {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }
        // instantiate PowerUp objects according to recorded turret positions from turret prefab
        {
            foreach (Vector3 powerupPosition in powerupPositions)
            {
                GameObject powerup = Instantiate(powerupPrefab, powerupPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                powerup.name = "Powerup";
                powerup.transform.parent = powerupHolder;
            }
        }

        //print system date and time of level load
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


