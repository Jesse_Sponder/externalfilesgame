﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //set up string reference to file path for save data in streamingassets folder and create a new list for containing save data (in strings)
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }

    //adds string "item" to saveItems list
    public void AddObject(string item) {
        saveItems.Add(item);
    }

    public void Save() {
        saveItems.Clear();
        //finds all active objects of type "Save" and adds them to saveItems after serializing them
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }
        //creates a txt file and writes each string entery in the saveItems list into the file
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }
    //clears saveItems list of entries and reloads current scene
    public void Load() {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






    //when a scene is loaded retrieve and load save game data, destroy the duplicate objects in teh scene and create objects from save file in that order
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    //reading save file with streamReader, as long as there is still an entry to read and that entry is not empty stores trimmed string in saveItems list
    void LoadSaveGameData() {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }
    //find each object in the scene which possesses a save script and destroy it
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }
    //determines where the verbatim prefabName occurs in the save entry, determines the starting point of the name and the end point.
    //Using these index points it extracts the prefab name from the string and uses it to create a new prefab of the same name from the resource folder
    //finally, sends deserialize message to object with saveItem entry reference allowing it to insert saved values 
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
