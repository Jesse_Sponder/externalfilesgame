﻿using System;

using UnityEngine;
[Serializable]
public class PowerupSave : Save {

    //Inherits from save class
    public Data data;
    private powerup power;
    private string jsonString;

    // serializable public class for storing values, inherits from BaseData
    [Serializable]
    public class Data : BaseData
    {
        public Color spriteColor;
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 scale;
        public float currentStep;
    }
    //retrieves tank reference and initializes data variable
    void Awake()
    {
        power = GetComponent<powerup>();
        data = new Data();
    }
    //overrides Serialize string from Save class. sets variables of data instance and converts to Json formatted string
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.scale = power.transform.localScale;
        data.position = power.transform.position;
        data.eulerAngles = power.transform.eulerAngles;
        data.spriteColor = power.sRenderer.color;
        data.currentStep = power.currentStep;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //overrides Deserialize function of Save class, overwrites values of data class with values retrieved ffrom json string
    //sets variables for prefab "Tank" to that of data instance
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        power.transform.position = data.position;
        power.transform.localScale = data.scale;
        power.transform.eulerAngles = data.eulerAngles;
        power.sRenderer.color = data.spriteColor;
        power.currentStep = data.currentStep;
        power.name = "Powerup";
    }
}
