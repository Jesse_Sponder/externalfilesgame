﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    //Inherits from save class
    public Data data;
    private Tank tank;
    private string jsonString;

    // serializable public class for storing values, inherits from BaseData
    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }
    //retrieves tank reference and initializes data variable
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }
    //overrides Serialize string from Save class. sets variables of data instance and converts to Json formatted string
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //overrides Deserialize function of Save class, overwrites values of data class with values retrieved ffrom json string
    //sets variables for prefab "Tank" to that of data instance
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}