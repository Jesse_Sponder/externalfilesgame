﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerup : MonoBehaviour
{

    public float speedMultiplier, duration;

    //change scale, colour, rotation
    public float currentStep;

    public SpriteRenderer sRenderer;

    private void Awake()
    {
        sRenderer = this.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        AnimateValues();
    }


    void AnimateValues()
    {
        currentStep += Time.deltaTime;
        if (currentStep >= 5)
        {
            currentStep = 0;
            RandomizeAttributes();
        }

    }
    void RandomizeAttributes()
    {
        
        sRenderer.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
        Debug.Log(sRenderer.color);
        transform.localScale = new Vector3 (Random.Range(2, 4), Random.Range(2, 4), 1);
        transform.eulerAngles = new Vector3(0,0,Random.Range(0,360));
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Tank playerTank = collision.GetComponent<Tank>();
            playerTank.SpeedUp(duration, speedMultiplier);

            Destroy(gameObject);
        }
    }
}
